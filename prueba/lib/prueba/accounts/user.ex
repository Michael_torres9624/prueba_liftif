defmodule Prueba.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset


  schema "users" do
    field :address, :string
    field :city, :string
    field :country, :string
    field :email, :string
    field :name, :string
    field :password, :string
    field :phone, :string

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:name, :email, :password, :phone, :country, :city, :address])
    |> validate_required([:name, :email, :password, :phone, :country, :city, :address])
  end
end
