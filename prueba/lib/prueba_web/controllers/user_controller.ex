defmodule PruebaWeb.UserController do
  use PruebaWeb, :controller
  # use AMQP

  alias Prueba.Accounts
  alias Prueba.Accounts.User

  action_fallback PruebaWeb.FallbackController

  def index(conn, _params) do
    users = Accounts.list_users()
    render(conn, "index.json", users: users)
  end

  def create(conn, %{} = user_params) do
    with {:ok, %User{} = user} <- Accounts.create_user(user_params) do
      conn
      |> put_status(:created)
      # rabbit_settings = Application.get_env :prueba, :rabbitmq
      # {:ok, connection} = AMQP.Connection.open(rabbit_settings)
      # {:ok, channel} = AMQP.Channel.open(connection)


      # AMQP.Connection.close(connection)
        |> render("show.json", user: user)
    end
  end

  def show(conn, %{"id" => id}) do
    user = Accounts.get_user!(id)
    render(conn, "show.json", user: user)
  end

  def update(conn, %{"id" => id, "user" => user_params}) do
    user = Accounts.get_user!(id)

    with {:ok, %User{} = user} <- Accounts.update_user(user, user_params) do
      render(conn, "show.json", user: user)
    end
  end

  def delete(conn, %{"id" => id}) do
    user = Accounts.get_user!(id)
    with {:ok, %User{}} <- Accounts.delete_user(user) do
      send_resp(conn, :no_content, "")
    end
  end
end
