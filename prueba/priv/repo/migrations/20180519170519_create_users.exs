defmodule Prueba.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :name, :string
      add :email, :string
      add :password, :string
      add :phone, :string
      add :country, :string
      add :city, :string
      add :address, :string

      timestamps()
    end

  end
end
